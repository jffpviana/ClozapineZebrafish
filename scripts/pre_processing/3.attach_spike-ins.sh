#Attach spike-in sequences to reference genome and corresponding annotation

$directory_refgen=$1

cd $1 #change to reference genome folder project folder


#see how many transcripts there are on the spike-in control files
cat ERCC92.fa | grep \> | wc -l # spike-in sequences can be downloaded from the manufacturer's website: https://www.thermofisher.com/order/catalog/product/4456740

#Add the ERCC92 information to the annotation file created with 2.download_reference_genome.sh script
cat zebrafish_mapping.txt ERCC92_mapping.txt > zebrafish_ERCC92_mapping.txt

#Just to check that all is ok, type
head zebrafish_ERCC92_mapping.txt

#Check to see if the spike-in IDs are added correctly at the end of the .txt file
tail zebrafish_ERCC92_mapping.txt


#append the spike-in sequences to the ref genome fasta file 
cat ERCC92.fa Danio_rerio.GRCz11.cdna.all.fa > Danio_rerio.GRCz11.cdna.all.spikein.fa
#check if number of rows of the files match
wc Danio_rerio.GRCz11.cdna.all.spikein.fa
wc Danio_rerio.GRCz11.cdna.all.fa
wc ERCC92.fa
