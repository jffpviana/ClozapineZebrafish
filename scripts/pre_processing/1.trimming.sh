#Trimming raw reads using Trimmomatic

##Explanation of trimmomatic arguments:

##Remove adapters (give the correct verio of the adapters) (ILLUMINACLIP:TruSeq3-PE.fa:2:30:30)

##Remove leading low quality or N bases (below quality 3) (LEADING:3)

##Remove trailing low quality or N bases (below quality 3) (TRAILING:3)

##Scan the read with a 4-base wide sliding window, cutting when the average quality per base drops below 15 (SLIDINGWINDOW:4:15)

##Cut the specified number of bases from the start of the read (HEADCROP:12)

##Drop reads below the 35 bases long (MINLEN:35)




#arguments set in the bash script
directory_rawdata=$1 #pathway to raw data
trimmo=$2 #pathway to Trimmomatic .jar file
adapters=$3 #pathway to adapters TruSeq3-PE.fa file 

cd $directory_rawdata #change to raw data directory

for i in {1..64..1} #For each sample, read both reads fastq files and run trimmomatic
do
read1=${i}_R1_999.fastq;
read2=${i}_R2_999.fastq;


java -jar $trimmo PE -threads 8 -phred33 $read1 $read2 $read1"_trimmed_paired.fastq" $read1"_trimmed_unpaired.fastq" $read2"_trimmed_paired.fastq" $read2"_trimmed_unpaired.fastq" ILLUMINACLIP:$adapters:2:30:30 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 HEADCROP:12 MINLEN:35
=======
java -jar ~/software/Trimmomatic-0.38/Trimmomatic-0.38/trimmomatic-0.38.jar PE -threads 8 -phred33 $read1 $read2 $read1"_trimmed_paired.fastq" $read1"_trimmed_unpaired.fastq" $read2"_trimmed_paired.fastq" $read2"_trimmed_unpaired.fastq" ILLUMINACLIP: ~/software/Trimmomatic-0.38/Trimmomatic-0.38/adapters/TruSeq3-PE.fa:2:30:30 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 HEADCROP:12 MINLEN:35

done


