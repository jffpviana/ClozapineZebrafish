#Aligning trimmed reads to the reference genome with bowtie2 within RSEM 

#arguments set in the bash script
directory_rawdata=$1 #pathway to raw data
resem=$2 #pathway to RSEM calculate expression script
ref_gen=$3 #pathway to reference genome
bowtie2=$4 #path to bowtie2

cd $directory_rawdata #change to raw data directory


for i in {1..64..1} #For each sample, read both reads TRIMMED fastq files and aligned using bowtie2 within RSEM
do
read1=${i}_R1_999.fastq_trimmed_paired.fastq;
read2=${i}_R2_999.fastq_trimmed_paired.fastq;


perl $resem -p 8 --paired-end $read1 $read2 $ref_gen --bowtie2 --bowtie2-path $bowtie2 $read1"_quals"

done

