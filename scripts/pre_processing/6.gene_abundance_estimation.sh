
#Abundance estimation using RSEM

#arguments set in the bash script
trinity=$1 #path to trinity software folder
abundance_script=$2 #path to estimate abundance script
directory_rawdata=$3 #path to raw data folder
mapping=$4 #path to mapping .txt 

cd $trinity #change directory to trinity software directory
wget https://github.com/trinityrnaseq/trinityrnaseq/blob/master/util/abundance_estimates_to_matrix.pl #download perl scripts needed
wget https://github.com/trinityrnaseq/trinityrnaseq/blob/master/util/run_TMM_scale_matrix.pl



cd $directory_rawdata #change directory


# Rename all sample file names so they are not so long (this could also be implemented in the 5.aligning.sh script)
rename '_R1_999.fastq_trimmed_paired.fastq_quals.' '' *


#estimate gene abundance from aligned reads using RSEM

perl $abundance_script --est_method RSEM --gene_trans_map $mapping 1isoforms.results 2isoforms.results 3isoforms.results 4isoforms.results 5isoforms.results 6isoforms.results 7isoforms.results 8isoforms.results 9isoforms.results 10isoforms.results 11isoforms.results 12isoforms.results 13isoforms.results 14isoforms.results 15isoforms.results 16isoforms.results 17isoforms.results 18isoforms.results 19isoforms.results 20isoforms.results 21isoforms.results 22isoforms.results 23isoforms.results 24isoforms.results 25isoforms.results 26isoforms.results 27isoforms.results 28isoforms.results 29isoforms.results 30isoforms.results 31isoforms.results 32isoforms.results 33isoforms.results 34isoforms.results 35isoforms.results 36isoforms.results 37isoforms.results 38isoforms.results 39isoforms.results 40isoforms.results 41isoforms.results 42isoforms.results 43isoforms.results 44isoforms.results 45isoforms.results 46isoforms.results 47isoforms.results 48isoforms.results 49isoforms.results 50isoforms.results 51isoforms.results 52isoforms.results 53isoforms.results 54isoforms.results 55isoforms.results 56isoforms.results 57isoforms.results 58isoforms.results 59isoforms.results 60isoforms.results 61isoforms.results 62isoforms.results 63isoforms.results 64isoforms.results --out_prefix genes_counts_ALL
