#Prepare reference genome for aligning using bowtie2 within RSEM

#arguments set in the bash script
directory_refgen=$1 #pathway to reference genome folder
rsem=$2 #pathway to RSEM prepare reference script 
ref_gen=$3 #path to reference genome and spike-ins fasta file
mapping=$4 #path to mapping .txt file
bowtie2=$5 #path to bowtie2

cd $directory_refgen #change to raw data directory


perl $rsem $ref_gen GRCz11 --transcript-to-gene-map $mapping --bowtie2 --bowtie2-path $bowtie2
