#Download zebrafish cDNA transcript genome from EMSEMBLE and extract gene and transcript annotation

$directory_refgen=$1

cd $1 #change to reference genome folder project folder

#Download zebrafish cDNA transcript genome from EMSEMBLE and extract gene and transcript annotation
wget ftp://ftp.ensembl.org/pub/release-92/fasta/danio_rerio/cdna/Danio_rerio.GRCz11.cdna.all.fa.gz

#unzip the file
gunzip -d Danio_rerio.GRCz11.cdna.all.fa.gz

#see how many transcripts there are
cat Danio_rerio.GRCz11.cdna.all.fa | grep \> | wc -l


#Extract gene and transcript IDs and save in separate .txt file
less  Danio_rerio.GRCz11.cdna.all.fa | grep \> | sed 's/ /\t/g' | cut -f1 | sed 's/>//g' > zebrafish_ensembl_transcript_IDs #transcript IDs
less  Danio_rerio.GRCz11.cdna.all.fa | grep \> | sed 's/gene:/\t/g' | cut -f2 | sed 's/ /\t/g' | cut -f1 > zebrafish_ensembl_gene_IDs #gene IDs
paste zebrafish_ensembl_gene_IDs zebrafish_ensembl_transcript_IDs > zebrafish_mapping.txt #combine both

