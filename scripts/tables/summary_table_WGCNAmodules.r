
args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out 
args[2]->directory_de
args[3]->softPower
args[4]->network.type 

network_type ->network.type 


library('WGCNA')
library(goseq)
library(org.Dr.eg.db) # Genome wide annotation for Zebrafish, primarily based on mapping using Entrez Gene identifiers.
#the following link has info on how to extract information from orgDB objects   
#https://www.bioconductor.org/help/workflows/annotation/annotation/#OrgDb

load(paste0(directory_out, "WGCNA_network_DE_", network.type, softPower, "_.Rdata")) #load the R object with the network and the list of genes with each corresponding network module

load(paste0(directory_de, "DESeq2_objects_LRT_DMSO_high_low_results.RData")) #load object and table of DE results
gsub("\\..*","",rownames(de_results_fcs))->rownames(de_results_fcs) #strip the _number after the ENSEMBL names of the results table


load(paste0(directory_out, "WGCNA_connectivities_DE_", network.type, softPower, "_.Rdata")) # load the R object with the connectivities for all genes

read.csv(paste0(directory_de, "ZF_human_orthology_150319.csv"), stringsAsFactors=FALSE) ->genesymbols #load human orthology information 
read.csv(paste0(directory_out,"WGCNA_ANOVA_connectivities_DE_", network.type, softPower, "_.csv"), stringsAsFactors=FALSE) ->anovas_connect #load ANOVA connectivities file

gsub("\\..*","",rownames(colorsmod))->rownames(colorsmod) #strip the .number after the ENSEMBL gene names
gsub("\\..*","",rownames(AlldegreesDMSO))->rownames(AlldegreesDMSO) #strip the .number after the ENSEMBL gene names
gsub("\\..*","",rownames(AlldegreesLOW))->rownames(AlldegreesLOW) #strip the .number after the ENSEMBL gene names
gsub("\\..*","",rownames(AlldegreesHIGH))->rownames(AlldegreesHIGH) #strip the .number after the ENSEMBL gene names

colorsmod[-which(colorsmod[,1]=="0"), , drop=FALSE]->colorsmod_nogrey #remove genes in the grey module from the network modules object


#ANOVA for each module#

mat_mod<-matrix(NA, length(unique(colorsmod_nogrey[,1])), 7)
colnames(mat_mod)<-c("Module","Module colour","N Probes","ANOVA P Value","Mean connectivity DMSO", "Mean connectivity Low",  "Mean connectivity High")

as.character(anovas_connect[,"Module"])->mat_mod[,"Module"]
as.character(anovas_connect[,"Colour"])->mat_mod[,"Module colour"]
tab_modules[-1,2]->mat_mod[,"N Probes"]
as.character(anovas_connect[,"ANOVA_p.value"])->mat_mod[,"ANOVA P Value"]

for(i in sort(unique(colorsmod_nogrey[,1]))){
mean(AlldegreesDMSO[rownames(colorsmod_nogrey)[which(colorsmod_nogrey[,1]==i)],"kWithin"])->mat_mod[i, "Mean connectivity DMSO"]

mean(AlldegreesLOW[rownames(colorsmod_nogrey)[which(colorsmod_nogrey[,1]==i)],"kWithin"])->mat_mod[i, "Mean connectivity Low"]

mean(AlldegreesHIGH[rownames(colorsmod_nogrey)[which(colorsmod_nogrey[,1]==i)],"kWithin"])->mat_mod[i, "Mean connectivity High"]
}


write.csv(mat_mod, paste0(directory_out, "WGCNA_network_DE_", network.type, softPower, "_summarymodules.csv"))