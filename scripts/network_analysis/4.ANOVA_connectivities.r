#Performs an ANOVA test on the connectivities for each module by exposure group.
#Inputs the R object with the network created with the 2.generate_network.r script and the R object with the gene correlation and connectivities created using the 3.calculate_connectivities.r script.
#Outputs a .csv file with the ANOVA p-value for each module.


args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out 
args[2]->directory_de
args[3]->softPower
args[4]->network.type 
args[5]->min_mod_size


library('WGCNA')

enableWGCNAThreads(10) #enables parallel calculation, determine number of processors depending on your system

cor = WGCNA::cor

load(paste0(directory_out, "WGCNA_network_DE_", network.type, softPower, "_.Rdata")) #load the R object with the network and the list of genes with each corresponding network module

load(paste0(directory_out,"WGCNA_connectivities_DE_", network.type, softPower, "_.Rdata")) # load the R object with the connectivities for all genes

colorsmod[-which(colorsmod[,1]=="0"), , drop=FALSE]->colorsmod_nogrey #remove genes in the grey module from the network modules object
 
identical(rownames(AlldegreesDMSO), rownames(colorsmod_nogrey)) #make sure the genes in the counts and network modules object are on the same order

anova_mat<-matrix(NA, nrow(tab_modules[-which(tab_modules$colorsmod=="0"),]), 3) #make an emptry matrix for the p-values of ANOVA on the connectivities

colnames(anova_mat)<-c("Module", "Colour", "ANOVA_p-value") #change column names
anova_mat[,1] <- as.character(tab_modules[-which(as.character(tab_modules$colorsmod)==0),1]) #module names
anova_mat[,2] <- col_modules[-which(col_modules=="grey")]#module colours


for(i in as.numeric(anova_mat[,1])){ #does this for each module

c(rep("A", length(AlldegreesDMSO$kWithin[which(colorsmod_nogrey[,1]==i)])),rep("B", length(AlldegreesLOW$kWithin[which(colorsmod_nogrey[,1]==i)])),rep("C", length(AlldegreesHIGH$kWithin[which(colorsmod_nogrey[,1]==i)])))->vec # bind the exposure groups (A,B and C) for the number of genes in this module, to imput as factors in the ANOVA model

c(AlldegreesDMSO$kWithin[which(colorsmod_nogrey[,1]==i)],AlldegreesLOW$kWithin[which(colorsmod_nogrey[,1]==i)],AlldegreesHIGH$kWithin[which(colorsmod_nogrey[,1]==i)])->mes #bind the connectivity values for each exposure group for the genes in this module, to imput in the ANOVA model

lm(mes~vec)->model #linear regression model
anova(model)->anovm #ANOVA
unlist(anovm[5])[1]->anova_mat[i,3] #extract p-values

}

write.csv(anova_mat, paste0(directory_out,"WGCNA_ANOVA_connectivities_DE_", network.type, softPower, "_.csv")) #save the matrix with the ANOVA p-values in a .csv file
