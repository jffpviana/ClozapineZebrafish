#Performs pathway enrichment analysis taking gene length bias into account using the GOseq package.
#Inputs the R object with the network created with the 2.generate_network.r script and the R object with the average length across transcripts created with the 5.gene_length.r script.
#Outputs a .csv file for each network module with the pathway enrichment analysis results. 

args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out 
args[2]->softPower
args[3]->network_type 


library('WGCNA')
library(goseq)
library(org.Dr.eg.db) # Genome wide annotation for Zebrafish, primarily based on mapping using Entrez Gene identifiers.
#the following link has info on how to extract information from orgDB objects   
#https://www.bioconductor.org/help/workflows/annotation/annotation/#OrgDb

load(paste0(directory_out, "WGCNA_network_DE_", network_type, softPower, "_.Rdata")) #load the R object with the network and the list of genes with each corresponding network module


load(paste0(directory_out, "Zebrafish_gene_lengths.RData")) #load gene lengths object

load(paste0(directory_out, "Zebrafish_dataset_GO_terms.RData")) #load GO terms object


gsub("\\..*","",rownames(colorsmod))->rownames(colorsmod) #strip the .number after the ENSEMBL gene names of the genes in the network

colorsmod[-which(colorsmod[,1]=="0"), , drop=FALSE]->colorsmod_nogrey #remove genes in the grey module from the network modules object
 
length_mat[match(rownames(colorsmod_nogrey),rownames(length_mat)),, drop=FALSE]->length_mat_nogrey #match the lengths to the genes in the modules of interest

 

for(i in sort(unique(colorsmod_nogrey[,1]))){ #do this for each module

vec<-rep(NA, nrow(colorsmod_nogrey)) #create an empty vector with the size of the current module
vec[which(as.numeric(colorsmod_nogrey[,1])==i)]<-1
vec[-which(as.numeric(colorsmod_nogrey[,1])==i)]<-0

nullp(vec, bias.data=as.numeric(length_mat_nogrey[,1]))->nullpdata #Fitting the Probability Weighting Function (PWF). This function obtains a weighting for each gene, depending on its length, given by the PWF

rownames(colorsmod_nogrey)->rownames(nullpdata) #give the same rownames to the PWF objects

goseq(nullpdata, gene2cat=list_go)->goseq #perform GOseq analysis
 
write.csv(goseq, paste0(directory_out, "GOseq_results_module",i,".csv")) #write results separate for each module on a .csv file
}


