#Extracts the gene ontology (GO) terms associated with the genes in this dataset.
#Inputs the R object with the network created with the 2.generate_network.r script.
#Outputs an R object with a list where each element is a gene in the dataset with the associated GO terms.
#The object package org.Dr.eg.db was used to extract the gene ontology (GO) terms associated with the zebrafish genes. This is likely to change and be updated with time. The results presented in this manuscript were generated on 27/06/2018


args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out 
args[2]->softPower
args[3]->network_type

#file names
"Zebrafish_dataset_GO_terms.RData" -> GOterms_file
 

library('WGCNA')
library(org.Dr.eg.db) # Genome wide annotation for Zebrafish, primarily based on mapping using Entrez Gene identifiers.
#the following link has info on how to extract information from orgDB objects   
#https://www.bioconductor.org/help/workflows/annotation/annotation/#OrgDb



load(paste0(directory_out, "WGCNA_network_DE_", network_type, softPower, "_.Rdata")) #load the R object with the network and the list of genes with each corresponding network module


gsub("\\..*","",rownames(colorsmod))->rownames(colorsmod)#strip the .number after the ENSEMBL gene names of the genes in the network


keys(org.Dr.eg.db, keytype="ENSEMBL")->ensemblgenes #extract the list of ENSEMBLE gene symbols from the org.Dr.eg.db object
select(org.Dr.eg.db,keys=ensemblgenes, columns="GO", keytype="ENSEMBL")->go_list #extract the GO terms associated to these genes


GO_mat<-matrix(NA, length(unique(go_list$GO)), 2) #create an empty matrix for unique GO terms
colnames(GO_mat) <- c("GO_term", "No_genes")#change column names
GO_mat[,1]<-unique(go_list$GO) #unique list of GO terms

 
for(i in 1:nrow(GO_mat)){ #do this for all the unique terms in the new matrix

	length(unique(go_list$ENSEMBL[which(go_list$GO==GO_mat[i,1])]))->GO_mat[i,2] #count te genes in that term and output the number in the new matrix
}


GO_mat[-which(as.numeric(GO_mat[,"No_genes"])<10),]->GO_mat_less #exclude GO terms with less than 10 genes (small terms that might not be representative)

go_list[which(go_list$GO%in%GO_mat_less[,"GO_term"]),]->go_list_less #map these GO terms back to the ENSEMBL gene symbols  

go_list_less[which(go_list_less$ENSEMBL%in%rownames(colorsmod)),]->GO_data #extract the GO terms that are represented in the genes present in the dataset

paste("Number of GO terms in dataset that will be tested:", length(unique(GO_data$GO))) #This gives us the number of all the GO terms present in the dataset and that are going to be tested. We will use this to calculate our bonferroni multiple testing correction threshold 

paste("Bonferroni threshold:", 0.05/length(unique(GO_data$GO))) #This calculates our multiple testing threshold
#3.34001336005344e-05


list_go<-list(GO_data$GO[which(GO_data$ENSEMBL==rownames(colorsmod)[1])]) #start a list of lists where each list represents one gene in the dataset and each element of the list is a GO term annotated to that gene

for(i in 2:length(rownames(colorsmod))){ #continue for the next GO terms
	append(list_go, list(as.character(GO_data$GO[which(GO_data$ENSEMBL==rownames(colorsmod)[i])])))->list_go #append to the previous
}

rownames(colorsmod)->names(list_go) #name the list elements the genes

save(list_go, file=paste0(directory_out, GOterms_file))