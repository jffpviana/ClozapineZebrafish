#Generates a correlation network with all the samples (except the water samples as we are only using DMSO as the true control).
#Uses the soft-thesholding power chosen from the plot created in the 1.calculate_soft_power.r script.
#Outputs an R object with the network created by the WGCNA  package and a list of genes with each corresponding network module.

args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out 
args[2]->directory_de
args[3]->softPower
args[4]->network_type 
args[5]->min_mod_size

#file names
"DESeq2_objects_LRT_DMSO_high_low_results.RData" -> counts_data

library('WGCNA')

enableWGCNAThreads(10) #enables parallel calculation, determine number of processors depending on your system

cor = WGCNA::cor

load(paste0(directory_de, counts_data)) #load object and table of DE results. This has the counts that were used in the analysis too. 
library(DESeq2)
log2(counts(de_exp, normalized=T)[rownames(de_results_fcs),]+1)->countslog #log transform the counts so they can be used by WGCNA 
detach("package:DESeq2", unload=TRUE)

rm(list=c("de_exp", "de_results_fcs", "deobj_exp")) #remove these objects to free up more memory

data<-t(countslog) #transpose betas (rows=samples, columns=counts)


network_pearson = blockwiseModules(data, power = softPower, TOMType = network_type, minModuleSize = min_mod_size, reassignThreshold = 0, mergeCutHeight = 0.25, numericLabels = TRUE, saveTOMs= FALSE, verbose = 3) #create the network

 
data.frame(network_pearson$colors)->colorsmod #extract the module each gene is in

rownames(colorsmod)<-rownames(countslog) #give the rows the names of the genes in the counts objects

data.frame(table(colorsmod))->tab_modules #create table with the number of genes in each module
labels2colors(as.numeric(as.vector(tab_modules[,1])))->col_modules #extract list of module colours. The colours are arbitrary attributed by WGCNA


save(network_pearson, colorsmod, tab_modules, col_modules, file = paste0(directory_out, "WGCNA_network_DE_", network_type, softPower, "_.Rdata")) # save the network and the list of genes with each corresponding network module in an R object

