#Calculate average length across transcripts for all zebrafish genes
#This needs to be done because GOseq had not been updated and the latest versions of some genomes are not recognised by the package.
#This is likely to change and be updated with time. The results presented in this manuscript were generated on 27/06/2018
#Outputs an R object with the average length across transcripts for all zebrafish genes


args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out 

#file names
"Zebrafish_gene_lengths.RData" -> gene_length_file


library(GenomicFeatures)

makeTxDbFromBiomart(biomart="ENSEMBL_MART_ENSEMBL", dataset="drerio_gene_ensembl")->txd_object #make a TxDb object using BioMart information on the latest version of the genome. Check that this object has the lastest version of the genome after creating it.


by_gene<-transcriptsBy(txd_object,"gene") # list object with different transcripts by gene
lengthData<-median(width(by_gene)) #the median transcript length of each gene 


as.matrix(lengthData)->length_mat #transform gene lengths into a matrix

colnames(length_mat)<-"Length" #change column name

#save gene lengths of ZF genome
save(length_mat, file=paste0(directory_out, gene_length_file))
