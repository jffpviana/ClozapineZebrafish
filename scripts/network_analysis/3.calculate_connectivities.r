#Calculates the absolute correlations and connectivities of all genes in the network separate by exposure group.
#Inputs the R object with the network created with the 2.generate_network.r script and the gene expression counts used to create the same network.
#Outputs an R object with the absolute correlations and another R object with the connectivities for all genes.


args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out 
args[2]->directory_de
args[3]->softPower
args[4]->network_type 
args[5]->min_mod_size

#file names
"DESeq2_objects_LRT_DMSO_high_low_results.RData" -> counts_data

library('WGCNA')

enableWGCNAThreads(10) #enables parallel calculation, determine number of processors depending on your system

cor = WGCNA::cor

load(paste0(directory_out, "WGCNA_network_DE_", network_type, softPower, "_.Rdata")) #load the R object with the network and the list of genes with each corresponding network module

load(paste0(directory_de, counts_data)) #load object and table of DE results. This has the counts that were used in the analysis too. 

library(DESeq2)
log2(counts(de_exp, normalized=T)[rownames(de_results_fcs),]+1)->countslog #log transform the counts so they can be used by WGCNA 
detach("package:DESeq2", unload=TRUE)

data.frame(colData(deobj_exp))->pheno # extract phenotype data used in analysis from the DESeq2 object

identical(colnames(countslog), rownames(pheno)) #make sure the samples in the counts and the phenotype objects are on the same order

identical(rownames(countslog), rownames(colorsmod)) #make sure the genes in the counts and network modules object are on the same order

#separate the counts by exposure group
countslog[,which(pheno$exp=="A")]->countsDMSO
countslog[,which(pheno$exp=="B")]->countsLOW
countslog[,which(pheno$exp=="C")]->countsHIGH


#remove genes in the module 0 from the counts objects and from the network modules object 
countsDMSO[-which(colorsmod[,1]=="0"),]->countsDMSO_nogrey
countsLOW[-which(colorsmod[,1]=="0"),]->countsLOW_nogrey
countsHIGH[-which(colorsmod[,1]=="0"),]->countsHIGH_nogrey

colorsmod[-which(colorsmod[,1]=="0"), , drop=FALSE]->colorsmod_nogrey

identical(rownames(countsDMSO_nogrey), rownames(colorsmod_nogrey)) #make sure the genes in the counts and network modules object are on the same order


####Calculate connectivities###
#DMSO
ADJDMSO=abs(cor(t(countsDMSO_nogrey),use="p"))^softPower #correlate all genes with all genes for the DMSO samples
AlldegreesDMSO=intramodularConnectivity(ADJDMSO, colorsmod_nogrey[,1]) # calaculate the connectivity for the genes in the DMSO samples

#LOW
ADJLOW=abs(cor(t(countsLOW_nogrey),use="p"))^softPower # correlate all genes with all genes for the LOW samples
AlldegreesLOW=intramodularConnectivity(ADJLOW, colorsmod_nogrey[,1]) # calaculate the connectivity for the genes in the LOW samples

#HIGH
ADJHIGH=abs(cor(t(countsHIGH_nogrey),use="p"))^softPower # correlate all genes with all genes for the HIGH
AlldegreesHIGH=intramodularConnectivity(ADJHIGH, colorsmod_nogrey[,1]) # calaculate the connectivity for the genes in the HIGH samples

#kTotal = connectivity with the network;   kWithin = connectivity to the module it was assigned to;  kOut = connectivity to the other modules;    kDiff = difference between within and out


##save connectivity
save(AlldegreesDMSO,AlldegreesLOW, AlldegreesHIGH, file=paste0(directory_out, "WGCNA_connectivities_DE_", network_type, softPower, "_.Rdata"))
save(directory_out, ADJDMSO,ADJLOW, ADJHIGH, file=paste0(directory_out, "WGCNA_abs_correlations_DE_", network_type, softPower, "_.Rdata"))