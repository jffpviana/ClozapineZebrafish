#Removes lowly expressed genes from the dataset
#Inputs the SeqExpressionSet object (EDASeq package) with the counts without spike-ins counts and phenotype information, created with the 7.counts_pheno_object.r script
#Outputs an R object with the counts of the expressed genes and phenotype information

args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out #directory to output files
args[2]->directory_pheno #directory with phenotype file

#file names
"counts_pheno_EDAseq.RData" -> counts_data
"counts-expressed_pheno_EDAseq.RData" -> output_data

library(EDASeq) #load EDASeq package

group_size<-16 #define group size to remove lowly expressed genes
min_reads<-10 #define minimum number of reads to keep genes


load(paste0(directory_out, counts_data)) #load SeqExpressionSet object (EDASeq package) with the counts without the spiek-ins and phenotype information, created with the 9.remove_spikes-ins.r script

round(counts(set))->counts_round # DESeq2 needs integer numbers so estimated counts from RSEM need to be rounded - this is because they are not raw gene counts, they were estimated from summing transcript counts

identical(colnames(counts_round),as.character(pheno$ID_Brain)) #check the counts and the phenotype file have the same order


#output total number of genes without spike-ins
paste("Total no. of genes:", nrow(counts_round))


# substitute number for names in case this is interfeering with the creation of the dummy variables in the model
pheno$Exposure[which(pheno$Exposure=="water")]<-"D" #this will be excluded for this analysis
pheno$Exposure[which(pheno$Exposure=="20")]<-"B"
pheno$Exposure[which(pheno$Exposure=="70")]<-"C"
pheno$Exposure[which(pheno$Exposure=="DMSO")]<-"A"

counts_round[,-which(pheno$Exposure=="D")]->counts_no_water #exclude water samples from counts dataframe
pheno[-which(pheno$Exposure=="D"),]->pheno_no_water #exclude water samples from phenotype matrix

identical(colnames(counts_round),as.character(pheno$ID_Brain)) #check the counts and the phenotype file have the same order


keep <- rowSums(counts_no_water >= min_reads) >= group_size #what genes have at least 'min_reads' number of reads in at least the 'group_size' number of samples
counts_expressed <- counts_no_water[keep, ] #extract only expressed genes from the counts dataset
counts_expressed_water <- counts_round[keep, ] #keep the same genes on the counts object that still contains the water samples for later use (when we want to look for differentially expressed genes between water and DMSO)


#output number of expressed genes
paste("Number of expressed genes:", nrow(counts_expressed))


#save a new R object
save(counts_expressed, pheno_no_water, counts_expressed_water, pheno, file=paste0(directory_out, output_data))