#Calculates differential expression between the three exposure groups of interest: DMSO, 20ug/L and 70ug/L
#Inputs the R object  with the counts without spike-ins counts or lowly expressed genes and phenotype information, created with the 8.remove_lowly_expressed.r script
#Outputs differential expression results in a DESeq2 object format and a .csv table.

args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out 
args[2]->directory_pheno

#file names
"counts-expressed_pheno_EDAseq.RData" -> counts_data
"LRT_DMSO_high_low_results.csv" -> output_data_csv
"DESeq2_objects_LRT_DMSO_high_low_results.RData" -> output_data

library("DESeq2") #load DESeq2 library

load(paste0(directory_out, counts_data)) #load EDASeq object (EDASeq package) with the counts without the spike-ins and phenotype information, created with the 8.remove_lowly_expressed.r script

identical(colnames(counts_expressed),as.character(pheno_no_water$ID_Brain)) #check the counts and the phenotype file have the same order


##first fit a model with NO COVARIATES in ALL OF THE SAMPLES so DESeq2 is able to identified outlying samples in specific genes. At this point DESeq2 isn't able to do this because once you add the covariates it considers the replicates groups in each of the covariates instead of the condition. It needs at least 7 replicates to be able to replace outlying samples with trimmed mean of the other samples.
#ouliers - The DESeq function calculates, for every gene and for every sample, a diagnostic test for outliers called Cook’s distance. Cook’s distance is a measure of how much a single sample is influencing the fitted coefficients for a gene, and a large value of Cook’s distance is intended to indicate an outlier count. The Cook’s distances are stored as a matrix available in assays(de_obj)[["cooks"]].

as.matrix(pheno_no_water$Exposure)->phenomodel #extract exposure information from phenotype object 
colnames(phenomodel)<-"exp" #change the single column name 

deobj_simple <- DESeqDataSetFromMatrix(countData = counts_expressed,
colData = phenomodel,
design = ~exp) #create DESeq object with model design information. In this case is just exposure groups to remove outliers

de_simple <- DESeq(deobj_simple, test="LRT", reduced=~1) #run model
assays(de_simple)[["replaceCounts"]]->counts_cooks 

#-- replacing outliers and refitting for 72 genes
#this is the output about the number of outliers that are replaced



###now perform your model with the covariates of interest 

as.factor(pheno_no_water$Gender)->sex #sex is a covariate
as.factor(pheno_no_water$Week_no)->week #week of experiment is a covariate
pheno_no_water$Exposure->exposure #exposure is independent variant of interest

data.frame(cbind(sex, week, exposure),stringsAsFactors=FALSE)->DE_pheno_model #create the model information with all the variates of interest
colnames(DE_pheno_model)<-c("sex", "week", "exp") #change column names
pheno_no_water$ID_Brain->rownames(DE_pheno_model) #change row names for samples names


#DESeq2 offers two kinds of hypothesis tests: the Wald test, where we use the estimated standard error of a log2 fold change to test if it is equal to zero, and the likelihood ratio test (LRT). The LRT examines two models for the counts, a full model with a certain number of terms and a reduced model, in which some of the terms of the full model are removed. The test determines if the increased likelihood of the data using the extra terms in the full model is more than expected if those extra terms are truly zero. The LRT is therefore useful for testing multiple terms at once, for example testing 3 or more levels of a factor at once, or all interactions between two variables. The LRT for count data is conceptually similar to an analysis of variance (ANOVA) calculation in linear regression, except that in the case of the Negative Binomial GLM, we use an analysis of deviance (ANODEV), where the deviance captures the difference in likelihood between a full and a reduced model.

####Do Likelihood ratio test

deobj_exp <- DESeqDataSetFromMatrix(countData = counts_cooks,
colData = DE_pheno_model,
design = ~sex + week + exp ) #create DESeq object with model design information. 

de_exp<- DESeq(deobj_exp , test="LRT", reduced=~sex + week)  #run model with all covariates
de_results <- results(de_exp, independentFiltering = FALSE) #Extract results of the model. switch off independent filtering as I already filtered before

cbind(results(de_exp, independentFiltering = FALSE, name="exp_B_vs_A")$log2FoldChange, results(de_exp, independentFiltering = FALSE, name="exp_B_vs_A")$lfcSE, results(de_exp, independentFiltering = FALSE, name="exp_C_vs_A")$log2FoldChange, results(de_exp, independentFiltering = FALSE, name="exp_C_vs_A")$lfcSE)->logfcs ##by default the function results() will print the fold change of the first vs last element (A vs C in this case), we need to extract both fold changes and correspondent lfcSE


data.frame(de_results[, -c(which(colnames(de_results)=="log2FoldChange"), which(colnames(de_results)=="lfcSE"))])->de_results_dataframe #excluse default column of fold change and SE because we need to add the two columns of fold change and two columns of SE for the two exposure groups of interest
cbind(de_results_dataframe,logfcs)->de_results_fcs #add the two columns of fold change for the two exposure groups of interest

colnames(de_results_fcs)<-c(colnames(de_results_dataframe),"logFC_DMSOvs20", "lfcSE_DMSOvs20", "logFC_DMSOvs70", "lfcSE_DMSOvs70") #change column names

de_results_fcs[order(de_results_fcs$pvalue),]->de_results_fcs #order by pvalue

write.csv(de_results_fcs, paste0(directory_out, output_data_csv)) #save csv file with results table ordered by p-value

save(deobj_exp, de_exp, de_results_fcs, file=paste0(directory_out, output_data)) #save object with DESeq object with replaced counts, design model and results
