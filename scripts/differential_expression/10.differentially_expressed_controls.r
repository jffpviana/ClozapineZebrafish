#Calculates differential expression between the two control groups: water and DMSO
#Inputs the R object  with the counts without spike-ins counts or lowly expressed genes and phenotype information, created with the 8.remove_lowly_expressed.r script
#Outputs differential expression results between the control groups in a .csv table.

args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out 
args[2]->directory_pheno

#file names
"counts-expressed_pheno_EDAseq.RData" -> counts_data
"Waldtest_DMSO_water_results.csv" -> output_data_csv
"DESeq2_objects_Waldtest_DMSO_water_results.RData" -> output_data

library("DESeq2")

load(paste0(directory_out, "counts-expressed_pheno_EDAseq.RData")) #load EDASeq object (EDASeq package) with the counts without the spike-ins and phenotype information, created with the 8.remove_lowly_expressed.r script

identical(colnames(counts_expressed_water),as.character(pheno$ID_Brain)) #check the counts and the phenotype file have the same order

counts_expressed_water[,c(which(pheno$Exposure=="D"), which(pheno$Exposure=="A"))]->counts_controls #extract only the two control groups from the counts object
pheno[c(which(pheno$Exposure=="D"), which(pheno$Exposure=="A")),]-> pheno_controls #extract only the two control groups from the phenotype object

identical(colnames(counts_controls),as.character(pheno_controls$ID_Brain)) #check the counts and the phenotype file have the same order


as.factor(pheno_controls$Gender)->sex #sex is a covariate
as.factor(pheno_controls$Week_no)->week #week of experiment is a covariate
pheno_controls$Exposure->exposure #exposure is independent variant of interest

data.frame(cbind(sex, week, exposure),stringsAsFactors=FALSE)->DE_pheno_model #create the model information with all the variates of interest
colnames(DE_pheno_model)<-c("sex", "week", "exp") #change column names
pheno_controls$ID_Brain->rownames(DE_pheno_model) #change row names for samples names



deobj_exp_controls <- DESeqDataSetFromMatrix(countData = counts_controls,
colData = DE_pheno_model,
design = ~sex + week + exp ) #create DESeq object with model design information. 

de_exp_controls<- DESeq(deobj_exp_controls)  #Wald test with DESeq2 with all covariates
de_results_controls <- results(de_exp_controls) #Extract results of the model. switch off independent filtering as I already filtered before


de_results_controls[order(de_results_controls$pvalue),]->de_results_controls #order by pvalue

write.csv(de_results_controls, paste0(directory_out, output_data_csv)) #save csv file with results table ordered by p-value

save(deobj_exp_controls, de_exp_controls, de_results_controls, file=paste0(directory_out, output_data)) #save object with DESeq object with replaced counts, design model and results

