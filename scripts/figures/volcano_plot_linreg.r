#This script plots a volcano plot of a differential expression analysis p-values and log fold changes

args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out
args[2]->DE_results
args[3]->type_analysis

read.csv(paste0(directory_out, DE_results), stringsAsFactors=FALSE)->DE_results

#remove NAs first (genes that were removed from the model due to low count by DESeq2)
if(length(which(is.na(DE_results$pvalue)))>0){

	DE_results[-which(is.na(DE_results$pvalue)),]->DE_results
	}else{

	}

#calculate log10 of p-values
-(log10(DE_results$pvalue))->minuslogpvalues



pdf(paste0(directory_out, type_analysis, "_volcano.pdf")) #open pdf

plot(DE_results$log2FoldChange, minuslogpvalues, xlab = "log(Fold change)", ylab = "-log10(P-value)", pch=1, ylim = c(0, max(minuslogpvalues)), xlim = c(min(DE_results$log2FoldChange), max(DE_results$log2FoldChange))) #plot -log10(p-values) against DMSOvs70 logFC


if(length(which(DE_results$padj<0.1))>0){
	points(DE_results$log2FoldChange[which(DE_results$padj<0.1)], minuslogpvalues[which(DE_results$padj<0.1)], pch=19, col="red")
	}else{
	}

dev.off() #close pdf 