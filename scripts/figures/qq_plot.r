#This script plots a qq-plot of a differential expression analysis p-values

args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out
args[2]->DE_results
args[3]->type_analysis

read.csv(paste0(directory_out, DE_results), stringsAsFactors=FALSE)->DE_results

#remove NAs first (genes that were removed from the model due to low count by DESeq2)
if(length(which(is.na(DE_results$pvalue)))>0){

	DE_results$pvalue[-which(is.na(DE_results$pvalue))]->pvalues
}else{
	DE_results$pvalue->pvalues

	}

#calculate log10 of pv-alues
-(log10(pvalues))->minuslogpvalues

# sort pvalues crescent order
o <- sort(minuslogpvalues,decreasing=F) #sort the -log10(p-values) by increasing order (more significant last)
e <- sort(-log10(ppoints(length(pvalues)))) #generate expected quantiles, also sorted 


osig <- sort(minuslogpvalues[which(DE_results$padj<0.1)],decreasing=F) #sort the -log10(p-values) by increasing order (more significant last)
esig <- sort(-log10(ppoints(length(pvalues)))[which(DE_results$padj<0.1)]) #generate expected quantiles, also sorted 



pdf(paste0(directory_out, type_analysis, ".pdf")) #open pdf

plot(e,o,xlab = "Expected quantiles", ylab = "Observed quantiles",  ylim = c(0, max(o)), xlim = c(0,max(e)), pch=".", col="red") #plot -log10(p-values) against expected quantiles
abline(a =0, b = 1) #plot straight diagonal line

points(esig, osig, pch=17, col="darkred")

dev.off() #close pdf