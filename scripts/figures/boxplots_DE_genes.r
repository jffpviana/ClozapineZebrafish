#Boxplots of differentially expressed genes (FDR 10%).

args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out
args[2]->DE_results
args[3]->gene_symbols

library(DESeq2)
library("beeswarm")

read.csv(paste0(directory_out, DE_results), stringsAsFactors=FALSE, row.names=1)->results
read.csv(paste0(directory_out, gene_symbols))->gene_symbols

load(paste0(directory_out, "DESeq2_objects_LRT_DMSO_high_low_results.RData")) #load object and table of DE results. This has the counts that were used in the analysis too. 



results[which(results$padj<0.10),]->anov5 # get FDR 10% genes
dim(anov5)
#12 8


log2(counts(deobj_exp)[rownames(anov5),]+1)->countsAnov #get counts for the FDR 10% genes
gsub("\\..*","",rownames(countsAnov))->rownames(countsAnov) #strip the _number after the ENSEMBL names of the results table


###order by DMSO, 20 and 70
data.frame(colData(deobj_exp))->phenomodel1
cbind(countsAnov[,which(phenomodel1$exp=="A")],countsAnov[,which(phenomodel1$exp=="B")],countsAnov[,which(phenomodel1$exp=="C")])->countsAnov2
rbind(phenomodel1[which(phenomodel1$exp=="A"),],phenomodel1[which(phenomodel1$exp=="B"),],phenomodel1[which(phenomodel1$exp=="C"),])->pheno2

identical(colnames(countsAnov2),rownames(pheno2))


###substitute Inf (log2(0)) by NA
countsAnov2[countsAnov2==-Inf]<-NA


cairo_pdf(paste0(directory_out, "Boxplots_DE_FDR5_panel.pdf",sep=""), height=16, width =17)
par(mfrow=c(4,3), mar=c(4,8,1.5,1.5), mgp=c(5,1,0))

for( i in 1:nrow(countsAnov2)){
boxplot(countsAnov2[i,] ~ pheno2$exp , 
    outline = FALSE,     ## avoid double-plotting outliers, if any
      xlab=NA,ylab="Log2(normalized counts + 1)",xaxt="n", boxlwd=1.1, lwd=1.1, lty=1, medlwd=2.5,
	  ylim=c(min(countsAnov2[i,],na.rm=T)-0.1,max(countsAnov2[i,],na.rm=T)+0.5), cex.lab=2,  cex.axis=1.5)


axis(side=1, at=c(1,2,3), labels=c("DMSO", "Low", "High"),cex.axis=2, mgp=c(0,1.5,0))
signif(anov5[rownames(countsAnov2)[i],"pvalue"],digits=3)->pval
legend("topright", inset=0.005, bg="transparent", bty = "n", legend=bquote(italic(P)-value == .(pval)), cex=1.8)

unique(as.character(gene_symbols$Gene_name[which(gene_symbols$Gene_stable_ID==rownames(countsAnov)[i])]))->genei

legend("topleft", inset=0.005, cex=2, bg="transparent", bty = "n", legend=bquote(italic(.(genei))))


 
beeswarm(countsAnov2[i,] ~ pheno2$exp ,
    pch = 21, col=c("grey",  "dark blue", "red4") , bg = "#00000050",
    corral = "wrap", method="swarm", cex.axis=0.5, add=T)

}
dev.off()
