#Boxplots of the connectivities for each module by exposure group (low, high and control).
#Inputs the R object with the network created with the 2.generate_network.r script, the R object with the gene correlation and connectivities created using the 5.calculate_connectivities.r script and the P-values of the ANOVAs on the connectivities created with the 7.ANOVA_connectivities.r script.
#Outputs a .pdf file for each module with the boxplots.

args <- commandArgs(TRUE) # option to read arguments from bash script
args[1]->directory_out
args[2]->directory_de
args[3]->softPower
args[4]->network_type
args[5]->min_mod_size
args[6]->mod_sig


options(stringsAsFactors = FALSE) 

library("DESeq2")
library('WGCNA')


load(paste0(directory_out, "WGCNA_network_DE_", network_type, softPower, "_.Rdata")) #load the R object with the network and the list of genes with each corresponding network module

load(paste0(directory_out, "WGCNA_connectivities_DE_", network_type, softPower, "_.Rdata")) # load the R object with the connectivities for all genes

read.csv(paste0(directory_out, "WGCNA_ANOVA_connectivities_DE_", network_type, softPower, "_.csv"), stringsAsFactors=FALSE)->anova_pvals #load the .csv file with the P-values of the ANOVAs on the connectivities 


colorsmod[-which(colorsmod[,1]=="0"), , drop=FALSE]->colorsmod_nogrey #remove genes in the grey module from the network modules object
 
identical(rownames(AlldegreesDMSO), rownames(colorsmod_nogrey)) #make sure the genes in the counts and network modules object are on the same order


tab_modules[-which( tab_modules[,1]=="0"), , drop=FALSE]-> tab_modules_nogrey #remove genes in the grey module from the network modules information table
 

cairo_pdf(paste0(directory_out, "WGCNAconnectivities_boxplot_allsig_", network_type, softPower, "_.pdf"), width =17) #start the pdf file for this particular module
 
par(mfrow=c(3,6))
LETTERS[1]->current_letter


	for(i in mod_sig){
 par(mar=c(4,4,2,1.5))
 
		c(rep("A", length(AlldegreesDMSO$kWithin[which(colorsmod_nogrey[,1]==i)])),rep("B", length(AlldegreesLOW$kWithin[which(colorsmod_nogrey[,1]==i)])),rep("C", length(AlldegreesHIGH$kWithin[which(colorsmod_nogrey[,1]==i)])))->vec # bind the exposure groups (A,B and C) for the number of genes in this module, to imput as factors in the boxplots

		c(AlldegreesDMSO$kWithin[which(colorsmod_nogrey[,1]==i)],AlldegreesLOW$kWithin[which(colorsmod_nogrey[,1]==i)],AlldegreesHIGH$kWithin[which(colorsmod_nogrey[,1]==i)])->mes #bind the connectivity values for each exposure group for the genes in this module, to input in the boxplots

		#boxplot
		boxplot(as.numeric(mes) ~ as.factor(vec) , 
			outline = FALSE,     #avoid double-plotting outliers, if any
			  xlab=NA,ylab="Connectivity",xaxt="n", boxlwd=1.1, lwd=1.1, lty=1, medlwd=2.5,
			  ylim=c(min(as.numeric(mes),na.rm=T)-0.1,max(as.numeric(mes),na.rm=T)+10), col=c("grey","dark blue","red4")) #plot the boxplot of the values of the exposure groups
		title(current_letter, adj=0, cex=4)

		axis(side=1, at=c(1,2,3), labels=c("DMSO", "Low", "High"),cex.axis=1.2, mgp=c(0,1.5,0)) #plot the x-axis labels for each exposure group
		format(as.numeric(anova_pvals[i,"ANOVA_p.value"]),digits=3, scientific=TRUE)->genei
		legend("topright",inset=c(-0.001,0), cex=1.2, bg="transparent", bty = "n", legend=bquote(italic(P)-value == .(genei))) #plot the legend with the ANOVA p-value
		LETTERS[which(LETTERS==current_letter)+1]->	current_letter
	}

	 
	 dev.off() #close the pdf file
