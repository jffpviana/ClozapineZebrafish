# Clozapine-induced transcriptomic changes in the zebrafish brain

Joana Viana1, Nick Wildman2, Eilis Hannon1, Audrey Farbos2, Paul O’Neill2, Karen Moore2, Ronny van Aerle3, Greg Paull2, Eduarda Santos2, Jonathan Mill1 

1 University of Exeter Medical School, University of Exeter, Exeter, UK.
2 Biosciences, College of Life & Environmental Sciences, University of Exeter, Exeter, UK.
3 Centre for Environment, Fisheries and Aquaculture Science (Cefas), Weymouth, Dorset, UK


## Repository description
This repository contains the scripts used in data analyses in the manuscript mentioned above.

## Research questions
1) Are there individual gene expression changes in the brain after a short exposure to clozapine? 
2) Are there coordinated gene expression changes in the brain after a short exposure to clozapine? 
3) If yes, are these groups of genes enriched for particular Gene Ontology terms?

## Brief description of experimental setup
- Zebrafish were exposed to the following conditions: water (control), DMSO (vehicle), 20ug/L clozapine (low dose), 70ug/L clozapie (high dose)
- Two tanks per each exposure goup. Four fish in each tank tank (2 males, 2 females)
- 3 days exposure repeated in two consecutive weeks
- Total of 16 fish per exposure group
- Whole brain was extracted
- RNA from whole brain extracted using the AllPrep DNA/RNA Micro Kit  (Qiagen)
- mRNA-sequencing libraries were prepared using TruSeq Stranded mRNA Library Prep kit (Illumina) with ERCC spike-in controls.
- mRNA-sequencing was carried out on an Illumina HiSeq 2500 Sequencing System (Illumina), multiplexing 8 samples per lane, to generate 100 bp paired reads

## Description of data analysis design
For more detailed information see Methods section of manuscript.

**Data pre-processing**

Data pre-processing was performed in UNIX.
- The expression profiles of the ERCC spike-in sequences were analysed against the manufacturer’s expression values.

- Trimmomatic (v0.38) was used to pre-process the raw reads
- The raw and trimmed read were inspected using FastQC (v0.11.5).
- RSEM (v1.3.1, using bowtie2 v2.3.4.1) was used to align the pre-processed reads to the zebrafish complementary reference genome (GRCz11 (GCA_000002035.4) release 92, downloaded from Ensembl: ftp://ftp.ensembl.org/pub/release-92/fasta/danio_rerio/cdna/ .
- The ‘abundance_estimates_to_matrix.pl’ script in RSEM was used to generate a matrix of estimated counts for all samples. 


**Differential expression**

Differential expression analyses were performed in R3.5.1.
- Lowly expressed genes were removed (we defined expressed genes as having at least 10 reads in at least 16 samples).
- The ‘DESeq’ function from the R package DESeq2 (v1.14.1) was used to perform the differential expression analyses. Genes identified as having outliers based on their Cook’s distance are replaced with the trimmed mean over all samples.
- To identify clozapine exposure-associated gene expression changes we performed a likelihood ration test using exposure (DMSO, 20µg/L and 70µg/L), sex, week and gRUV normalisation factors as independent variables. 
- To see if clozapine dose interacts with sex we repeated the analysis adding the interaction term exposure*sex.
- To identify changes between the two control groups (water and DMSO), the expression values of these two groups for the same genes included in the main analysis were extracted. We then generated normalisation factors as described above and performed a Wald test (‘DESeq’ function, DESeq2) using exposure (water and DMSO), sex, week and gRUV normalisation factors as independent variables. 

**Network analysis**

Network analysis was performed in R.
- Network analysis on the gene expression values of thegenes that survived QC was performed using the R package WGCNA (v1.51).
- We created a signed network using the ‘blockwiseModules’ function (Pearson correlation, soft thresholding power = 12, minimum module size = 20, maximum block size = 5,000). The base 2 logarithms (log2) of the normalised counts + 1 of the exposure groups (excluding the water samples) were used. 
- The module 0 (‘grey’) containing non-allocated genes was excluded from further analysis. 
- The ‘moduleEigengenes’ function was used to calculate module eigengenes for each sample and we used a Student’s t-test to identify differences between males and females and between the two weeks of the experiment. 
- Connectivity values for each gene were calculated separately for the exposure groups using the ‘intramodularConnectivity’ function on the absolute correlations between genes raised to the power of 12. 
- We performed a one-way ANOVA using the ‘within module connectivity’ (kWithin) of the three exposure groups to identify significant changes in module connectivity with clozapine exposure. 

**Pathway analyses**

Pathway analyses were performed in R.
- We tested for enrichment of Gene Ontology (GO) terms in each of the modules showing connectivity changes with clozapine. 
- The R annotation package org.Dr.eg.db (v 3.3.0) was used to extract updated GO terms for GRCz10.
- GO terms with less than 10 genes in the dataset were excluded from analyses.
- The ‘makeTxDbFromBiomart’ function from the R package GenomicFeatures (v1.24.5) was used to extract the length of the genes in the analyses.
- For the genes allocated to each module, the ‘nullp’ and ‘goseq’ functions from the R package goseq (v1.24.0) were used to calculate the probability weighting function and test for GO term enrichment, respectively. 
- For the results of each module, we summarised the GO terms with *P*-value < 1.00E-03 using REVIGO (January 2017) (0.5 allowed similarity, whole UniProt database updated March 2017). 

You can find the phenotype file in the folder *other_files*

## Scripts description
This section describes the function of each script in this repository

**Pre-processing scripts** - *pre_processing* folder
- 1.trimming.sh - Trimming raw reads using Trimmomatic
- 2.download_reference_genome.sh - Download zebrafish cDNA transcript genome from EMSEMBLE and extract gene and transcript annonation
- 3.Attach spike-ins.sh - Attach spike-in sequences to reference genome and corresponding annotation
- 4.aligning.sh - aligning trimmed reads to the reference genome with RSEM 
- 5.sort_bams - Use samtools to sort mapped reads (BAM files)
- 6.gene_abundance_estimation - Abundance estimation using RSEM

**Differential expression scripts** - *differential_expression* folder
- 7.counts_pheno_object.r - Outputs an R object with the the EDASeq object (EDASeq package) with the counts and phenotype information
- 8.remove_lowly_expressed.r - Removes lowly expressed genes from the dataset
- 9.differential_expressed.r - Calculates differential expression between the three exposure groups of interest: DMSO, 20ug/L and 70ug/L
- 10.differential_expressed_controls.r - Calculates differential expression between the two control groups: water and DMSO
- 11.interaction_with_sex.r - Calculates exposure*sex interaction for the three exposure groups of interest: DMSO, 20ug/L and 70ug/L

**Network analysis scripts** - *network_analysis* folder
- 1.calculate_soft_power.r - Plots the scale-free topology fit index as a function of a soft-thresholding power
- 2.generate_network.r - Generates a correlation network with all the samples (except the water samples as we are only using DMSO as the true control)
- 3.calculate_connectivities.r - Calculates the absolute correlations and connectivities of all genes in the network separate by exposure group
- 4.ANOVA_connectivities.r - performs an ANOVA test on the connectivities for each module by exposure group
- 5.gene_length.r - calculate average length across transcripts for all zebrafish genes
- 6.extract_GO.r - extracts the gene ontology (GO) terms associated with the genes in this dataset
- 7.pathway_analysis.r - performs pathway enrichment analysis taking gene length bias into acount using the GOseq package

**Scripts for the figures of the manuscript** - *figures* folder
- boxplots_DE_genes.r - boxplots of differentially expressed genes (FDR 10%)
- qq_plot.r - this script plots a qq-plot of a differential expression analysis p-values
- volcano_plot_ANOVA.r - this script plots a volcano plot of a differential expression analysis p-values and log fold changes
- volcano_plot_linreg.r - this script plots a volcano plot of a differential expression analysis p-values and log fold changes
- network_connectivities_scatter.r - scatter plots of the connectivities for each module by exposure group (low/high versus control)
- network_connectivities_boxplots.r - boxplots of the connectivities for each module by exposure group (low, high and control)
